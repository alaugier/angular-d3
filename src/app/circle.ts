export interface Circle {
    id : number;
    name?: string;
    cx: number;
    cy: number;
    r: number;
    link?: number;
}

export interface Connections{
    source: number;
    destination: number;
}