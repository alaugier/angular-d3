import {
  AfterViewInit,
  Component,
  OnChanges,
  OnInit,
  ViewChild,
  ɵgetDebugNode__POST_R3__,
} from "@angular/core";
import { Circle } from "../circle";
import { CIRCLES, CONNECTIONS } from "../mock-circles";
import * as d3 from "d3";

@Component({
  selector: "app-circles",
  templateUrl: "./circles.component.html",
  styleUrls: ["./circles.component.css"],
})

export class CirclesComponent implements OnInit {
  
  svg: any;
  g: any;
  self = this;
  
  selecteditem?: any;
  ngOnInit(): void {
    this.svg = d3.select("#svg")
      .append('svg')
      .attr("viewBox", "0 0 2000 500")
      .attr("stroke-width", 2);
    // this.g = this.svg.append('g');

    d3.select('svg')
      .append("g")
      .selectAll('circle')
      .data(CIRCLES)
      .join('circle')
      .attr("cx", d => d.cx)
      .attr("cy", d => d.cy)
      .attr("r", d => d.r)
      .attr("fill", '#008ec4')
      .attr("stroke", "green")
      .attr("stroke-width", '0')

      // @ts-ignore
      .call(d3.drag().on('drag', function(event, d) {d3.select(this).attr("cx", d.x = event.x).attr("cy", d.y = event.y)} ))

      .on("click",       
        (d) =>
        {  
          d3.selectAll("circle")
          .attr("stroke-width", '0');
          d3.select(d.srcElement) //selects the DOM element that is clicked
          .attr("stroke-width", '2 %');
          this.selecteditem = d.target.__data__; // data of the DOM element
          d3.select('#toDisplay').text('Node ID : ' + this.selecteditem?.id);
        }
      )

      .on("mouseover",
       (d) => 
       { 
       d3.select(d.srcElement)
        .attr("r",d.srcElement.r.baseVal.value + (d.srcElement.r.baseVal.value/10)); //raises by 10% the size
       })
       .on("mouseout",
       (d) => 
       { 
        d3.select(d.srcElement)
        .attr("r",d.srcElement.r.baseVal.value - (d.srcElement.r.baseVal.value/10));
       })

  }





}