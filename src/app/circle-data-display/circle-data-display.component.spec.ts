import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CircleDataDisplayComponent } from './circle-data-display.component';

describe('CircleDataDisplayComponent', () => {
  let component: CircleDataDisplayComponent;
  let fixture: ComponentFixture<CircleDataDisplayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CircleDataDisplayComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CircleDataDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
