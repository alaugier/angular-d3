import { Component, Input, OnInit } from '@angular/core';
import { Circle } from '../circle';

@Component({
  selector: 'app-circle-data-display',
  templateUrl: './circle-data-display.component.html',
  styleUrls: ['./circle-data-display.component.css']
})
export class CircleDataDisplayComponent implements OnInit {

  @Input() selecteditem?: any;   //recoit un objet Circle 

  constructor() { }

  ngOnInit(): void {
  }

}
