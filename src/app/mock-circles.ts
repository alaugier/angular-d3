import { Circle } from './circle'
import { Connections} from './circle'

export const CIRCLES: Circle[] = [
    {
        id: 4,
        name: 'fourth',
        cx: 300,
        cy: 400,
        r: 20,
        link: 3
      },
    {
        id: 2,
        name: 'second',
        cx: 250,
        cy: 250,
        r: 50
    },
    {
        id: 3,
        name: 'third',
        cx: 250,
        cy: 50,
        r: 50
    },
    {
        id: 1,
        name: 'premier',
        cx: 50,
        cy: 50,
        r: 50
    }

];

export const CONNECTIONS: Connections[] =[
    {
        source: 1,
        destination: 4
    },
    {
        source: 3,
        destination: 1
    }
]