import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { CirclesComponent } from './circles/circles.component';
import { CircleDataDisplayComponent } from './circle-data-display/circle-data-display.component';
import { PathsComponent } from './paths/paths.component';

@NgModule({
  declarations: [
    AppComponent,
    CirclesComponent,
    CircleDataDisplayComponent,
    PathsComponent,

  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
